import { createSlice, PayloadAction } from '@reduxjs/toolkit';



const initialState = {
  value: '',
  isCollapse:false
};

export const GlobalSlice = createSlice({
  name: 'global',
  initialState,
  reducers: {
    setCollapse: (state, action) => {
      state.isCollapse = action.payload;
    }
  }
});

export const { setCollapse } = GlobalSlice.actions;
export const getValue = (state) => state.global.value;
export const getCollapse = (state) => state.global.isCollapse;

export default GlobalSlice.reducer;
